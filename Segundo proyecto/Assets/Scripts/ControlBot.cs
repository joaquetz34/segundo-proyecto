using UnityEngine;

public class ControlBot : MonoBehaviour

{
    private int hp;
    private GameObject jugador;
    public int rapidez;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaņo();
        }
    }
    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }


    void Start()
    {
        hp = 100;
        jugador = GameObject.Find("Jugador");

    }

    public void recibirDaņo()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }
}

